<?php
error_reporting(0);
require_once 'PHPExcel/IOFactory.php';
require_once 'PHPExcel.php';
$excel2 = PHPExcel_IOFactory::createReader('Excel5');
$excel2 = $excel2->load('Manifest.xls'); // Empty Sheet
$excel2->setActiveSheetIndex(0);
header('Content-Type: application/zip');
$index = intval(trim($_POST["index"]));

if(!(isset($_POST["cp"])&&isset($_POST["md"])&&isset($_POST["nbsi"]))){
	echo "Bad Request";
}

$company_name = trim($_POST["cp"]);
$job_name = trim($_POST["jn"]);
header("Content-Disposition: attachment; filename=\"".str_replace(" ", "_", $job_name).".zip\"");

$mail_description = trim($_POST["md"]);
$number_of_bundles = intval(trim($_POST["nbsi"]));
$delim =!isset($_POST["delim"])?",":$_POST["delim"]; 
$row_index = 4;
$columns = array(array("A","B","C"),array("D","E","F"));
$col_index = 0;
$total_bundles = 0;
$total_pieces = 0;
$csv_file   = "";
$merge = file_get_contents("slip.fdf");
$merge_file = array() ;
$home_date  = $_POST["home_date"];
for($i=0;$i<$index;$i++){
	if(!(isset($_POST["zc_".$i])&&isset($_POST["rn_".$i])&&isset($_POST["np_".$i])&&isset($_POST["co_".$i]))){
		continue;
	}
	$zip_code = trim($_POST["zc_".$i]);
	$route_number = trim($_POST["rn_".$i]);
	$no_pieces = intval(trim($_POST["np_".$i]));
	$co_office = trim($_POST["co_".$i]);
	$excel2->getActiveSheet()->setCellValue($columns[$col_index][0].$row_index, $zip_code);
	$excel2->getActiveSheet()->setCellValue($columns[$col_index][1].$row_index, $route_number);
	$excel2->getActiveSheet()->setCellValue($columns[$col_index][2].$row_index, $no_pieces);
	$total_pieces  = $total_pieces  + $no_pieces; 
	$j = 0;
	$nb = ($no_pieces-$number_of_bundles*$j)>$number_of_bundles?$number_of_bundles:($no_pieces-$number_of_bundles*$j);
	$output = "";
	$dummy = "%_KWED@?¿¿?->>><<<<<kdjfuy";
	while($nb>0){
		$output  = $output.$mail_description.$delim.$zip_code.$delim.$route_number.$delim.($j+1).$delim.$dummy.$delim.$nb.$delim.$company_name.$delim.$co_office.PHP_EOL;	
		$temp_merge = $merge;
		$temp_merge  = str_replace('%zip%',   	    $zip_code, $temp_merge);
		$temp_merge  = str_replace('%route%', 	    $route_number, $temp_merge);
		$temp_merge  = str_replace('%type%',  	    $mail_description, $temp_merge);
		$temp_merge  = str_replace('%home_date%',   $home_date, $temp_merge);
		$temp_merge  = str_replace('%tmob%',        $nb, $temp_merge);
		$temp_merge  = str_replace('%tob1%',        ($j+1), $temp_merge);
		$temp_merge  = str_replace('%tob2%',        $dummy, $temp_merge);
		$temp_merge  = str_replace('%description%', $job_name, $temp_merge);
		$temp_merge  = str_replace('%company%',     $company_name, $temp_merge);
		$temp_merge  = str_replace('%office%',      $co_office, $temp_merge);
		$merge_file[$zip_code."_".$route_number."_".($j+1).".fdf"] = $temp_merge; 
		$j = $j +1;
		$nb = ($no_pieces-$number_of_bundles*$j)>$number_of_bundles?$number_of_bundles:($no_pieces-$number_of_bundles*$j);	
	}
	foreach ($merge_file as $key => $value) {
		$merge_file[$key] = str_replace($dummy, $j, $value);
	}
	$csv_file = $csv_file.str_replace($dummy, $j, $output);
	$total_bundles = $total_bundles + $j;
	$col_index = $col_index + 1;
	if($col_index>1){
		$row_index = $row_index  + 1;
		$col_index = 0;
	}
}
unlink("temp.xls");
unlink("test.zip");
$excel2->getActiveSheet()->setCellValue("A31",$excel2->getActiveSheet()->getCell('A31')->getValue().$total_bundles);
$excel2->getActiveSheet()->setCellValue("C31",$excel2->getActiveSheet()->getCell('C31')->getValue().$total_pieces);
$excel2->getActiveSheet()->setCellValue("A32",$excel2->getActiveSheet()->getCell('A32')->getValue().$mail_description);
$excel2->getActiveSheet()->getStyle('A32')->getAlignment()->setWrapText(true);
$excel2->getActiveSheet()->setCellValue("B32",$excel2->getActiveSheet()->getCell('B32')->getValue().date("j/n/y H:i"));
$excel2->getActiveSheet()->getStyle('B32')->getAlignment()->setWrapText(true);
$objWriter = PHPExcel_IOFactory::createWriter($excel2, 'Excel5');

$objWriter->save('temp.xls');
$xls = file_get_contents('temp.xls');

#WRITE PDF
$file = $_POST["form_pdf"].".fdf";
$content = file_get_contents($file);
if($_POST["form_pdf"]=="3587"){
	$content = str_replace("%CRID%", trim($_POST["CRID"]), $content);
	$content = str_replace("%pieces%", $total_pieces, $content);
	$content = str_replace("%total_bundles%", $total_bundles, $content);
	$content = str_replace("%post_office%", trim($_POST["NameOfStation"]).", ".trim($_POST["state"]), $content);
	$content = str_replace("%weight%", trim($_POST["Weight"]), $content);
	$content = str_replace("%telephone%", trim($_POST["telephone"]), $content);
	$content = str_replace("%mailer%",$_POST["CompanyName"]."\r".$_POST["ContanctName"]."\r" .$_POST["StreetAdress"]."\r".$_POST["city"].", ".$_POST["state"]." ".$_POST["zip"], $content);
	$content = str_replace("%description%", $job_name, $content);
}

else{
	$content = str_replace("%home_date%", $home_date, $content);
	$content = str_replace("%order_number%", $job_name, $content);
	$content = str_replace("%station%", $_POST["Station_Name_8125"], $content);
	$content = str_replace("%15%", $_POST["Station_Name_8125"].", \"DDU\", ".$_POST["city_8125"].", ".$_POST["state_8125"]." ".$_POST["zip_8125"], $content);
	$content = str_replace("%16%", $_POST["CNMS"], $content);
	$content = str_replace("%phone%", $_POST["telephone_8125"], $content);
	$content = str_replace("%21%", $total_pieces, $content);
	$content = str_replace("%28%", $_POST["Station_Name_8125"]."\r".$_POST["Street_8125"]."\r".$_POST["city_8125"].", ".$_POST["state_8125"]." ".$_POST["zip_8125"], $content);
		
}

$zipFile = new ZipArchive();
$res = $zipFile->open('test.zip', ZipArchive::CREATE);
$zipFile->addFromString(str_replace(" ", "_", $job_name).".csv",$csv_file);
foreach ($merge_file as $key => $value) {
	$zipFile->addFromString($key,$value);
}
$zipFile->addFromString($_POST["form_pdf"].".fdf",$content);
$zipFile->addFile("temp.xls",str_replace(" ", "_", $job_name)."_manifest.xls");
$zipFile->addFile("slip.pdf","pdfs/slip.pdf");
$zipFile->addFile("3587.pdf","pdfs/3587.pdf");
$zipFile->addFile("8125.pdf","pdfs/8125.pdf");
$zipFile->close();
echo file_get_contents("test.zip");



?>
